//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "c_weapon__stubs.h"
#include "basehlcombatweapon_shared.h"
#include "c_basehlcombatweapon.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

STUB_WEAPON_CLASS( weapon_nh_hatchet, WeaponNHHatchet, C_BaseHLBludgeonWeapon );
STUB_WEAPON_CLASS( weapon_nh_pistol, WeaponNHPistol, C_BaseHLCombatWeapon );
STUB_WEAPON_CLASS( weapon_nh_revolver, WeaponNHRevolver, C_BaseHLCombatWeapon );
STUB_WEAPON_CLASS( weapon_nh_smg, WeaponNHSMG, C_HLSelectFireMachineGun );
STUB_WEAPON_CLASS( weapon_nh_shotgun, WeaponNHShotgun, C_BaseHLCombatWeapon );
